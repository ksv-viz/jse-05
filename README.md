# Task manager:
* Управление списком задач

# Software:
Product | Version
--- | ---
*java* | `11`
*maven* | `3.6`

# Developer
 serg.v.kazakov
 * serg.v.kazakov@mail.ru

# Build:
```
 mvn clean
```
```
 mvn clean package
```
```
 mvn clean install 
```

# Run:
## Windows:
```
 java -jar <artefact_folder>\task-manager-1.0.0.jar
```
## *nix:
```
 java -jar <artefact_folder>/task-manager-1.0.0.jar
```
